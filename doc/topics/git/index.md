---
stage: Create
group: Source Code
description: Common commands and workflows.
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments"
---

# Learn Git

Git is a [free and open source](https://git-scm.com/about/free-and-open-source)
distributed version control system. It handles projects of all sizes quickly and
efficiently, while providing support for rolling back changes when needed.

GitLab is built on top of (and with) Git, and provides you a Git-based, fully-integrated
platform for software development. GitLab adds many powerful
[features](https://about.gitlab.com/features/) on top of Git to enhance your workflow.

| | | |
|--|--|--|
| [**Get started**](get_started.md) **{chevron-right}**<br><br>Overview of how features fit together. | [**Install Git**](how_to_install_git/index.md) **{chevron-right}**<br><br>Download, configuration, system requirements. | [**Tutorial: Create your first commit**](../../tutorials/make_first_git_commit/index.md) **{chevron-right}**<br><br>Initial commit, Git basics, repository setup. |
| [**Clone a repository to your local machine**](clone.md) **{chevron-right}**<br><br>Local repository, clone, remote repository, SSH. | [**Create a branch for your changes**](branch.md) **{chevron-right}**<br><br>Branching, branch switch, checkout. | [**Add files to your branch**](add_files.md) **{chevron-right}**<br><br>Git add, staging changes, file management, commits. |
| [**Stash changes for later**](stash.md) **{chevron-right}**<br><br>Temporary storage, work in progress, context switching. | [**Undo changes**](undo.md) **{chevron-right}**<br><br>Reverting commits, removing changes, Git reset, unstage. | [**Tutorial: Update Git commit messages**](../../tutorials/update_commit_messages/index.md) **{chevron-right}**<br><br>Commit message editing, version history, best practices. |
| [**Rebase to address merge conflicts**](git_rebase.md) **{chevron-right}**<br><br>Conflict resolution, rebase, branch management. | [**Common Git commands**](commands.md) **{chevron-right}**<br><br>Git cheatsheet, basic operations, command line. | [**Tutorial: Update Git remote URLs**](../../tutorials/update_git_remote_url/index.md) **{chevron-right}**<br><br>Change the push/pull URL on a working copy. |
| [**Troubleshooting**](troubleshooting_git.md) **{chevron-right}**<br><br>Error resolution, common issues, debugging, Git problems. | | |
