---
stage: Secure
group: Static Analysis
description: Container, dependency, and vulnerability scans.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Secure your application

GitLab can check your applications for security vulnerabilities.

| | | |
|--|--|--|
| [**Getting started**](get-started-security.md) **{chevron-right}**<br><br>Overview of how features fit together. | [**Application security**](index.md) **{chevron-right}**<br><br>Scanning, vulnerabilities, compliance, customization, reporting. | [**Security configuration**](configuration/index.md) **{chevron-right}**<br><br>Configuration, testing, compliance, scanning, enablement. |
| [**Container Scanning**](container_scanning/index.md) **{chevron-right}**<br><br>Image vulnerability scanning, configuration, customization, reporting. | [**Dependency Scanning**](dependency_scanning/index.md) **{chevron-right}**<br><br>Vulnerabilities, remediation, configuration, analyzers, reports. | [**Comparison**](comparison_dependency_and_container_scanning.md) **{chevron-right}**<br><br>Dependency Scanning compared to Container Scanning. |
| [**Dependency List**](dependency_list/index.md) **{chevron-right}**<br><br>vulnerabilities, licenses, filtering, exporting. | [**Continuous Vulnerability Scanning**](continuous_vulnerability_scanning/index.md) **{chevron-right}**<br><br>Scanning, dependencies, advisories, background jobs. | [**Static Application Security Testing**](sast/index.md) **{chevron-right}**<br><br>Scanning, configuration, analyzers, vulnerabilities, reporting, customization, integration. |
| [**Infrastructure as Code (IaC) Scanning**](iac_scanning/index.md) **{chevron-right}**<br><br>Vulnerability detection, configuration analysis, pipeline integration. | [**Secret detection**](secret_detection/index.md) **{chevron-right}**<br><br>Detection, prevention, monitoring, storage, revocation, reporting. | [**Dynamic Application Security Testing (DAST)**](dast/index.md) **{chevron-right}**<br><br>Automated penetration testing, vulnerability detection, web application scanning, security assessment, CI/CD integration. |
| [**API Security**](api_security/index.md) **{chevron-right}**<br><br>Protection, analysis, testing, scanning, discovery. | [**Web API Fuzz Testing**](api_fuzzing/index.md) **{chevron-right}**<br><br>Testing, security, vulnerabilities, automation, errors. | [**Coverage-guided fuzz testing**](coverage_fuzzing/index.md) **{chevron-right}**<br><br>Coverage-guided fuzzing, random inputs, unexpected behavior. |
| [**Security Dashboard**](security_dashboard/index.md) **{chevron-right}**<br><br>Security dashboards, vulnerability trends, project ratings, metrics. | [**Offline environments**](offline_deployments/index.md) **{chevron-right}**<br><br>Offline security scanning, resolving vulnerabilities. | [**Vulnerability Report**](vulnerability_report/index.md) **{chevron-right}**<br><br>Filtering, grouping, exporting, manual addition. |
| [**Vulnerability Page**](vulnerabilities/index.md) **{chevron-right}**<br><br>Vulnerability details, status, resolution, linking issues. | [**Vulnerability severity levels**](vulnerabilities/severities.md) **{chevron-right}**<br><br>Classification, impact, prioritization, risk assessment. | [**GitLab Advisory Database**](gitlab_advisory_database/index.md) **{chevron-right}**<br><br>Security advisories, vulnerabilities, dependencies, database, updates. |
| [**CVE ID requests**](cve_id_request.md) **{chevron-right}**<br><br>Vulnerability tracking, security disclosure. | [**Policies**](policies/index.md) **{chevron-right}**<br><br>Security policies, enforcement, compliance, approvals, scans. | [**Security scanner integration**](../../development/integrations/secure.md) **{chevron-right}**<br><br>Reporting, vulnerabilities, remediations, tracking. |
