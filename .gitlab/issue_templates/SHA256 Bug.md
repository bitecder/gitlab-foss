/title SHA256 Bug <!-- concise title here -->

### SHA256 Bug Summary

<!-- Summarize the bug -->

### Steps to reproduce

<!-- Describe how one can reproduce the buggy behavior. Please use an ordered
list -->

### Example project

<!-- If possible, provide a link to the project that is experiencing buggy
behavior -->
